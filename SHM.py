from numpy import pi
shm_answer = {'A':0.1,'w':pi/4,'phi':pi/2}
shm_answer2 = {'A':0.08,'w':6/(2*pi),'phi':-20*pi/180}


# # Graphing Circular Motion

# %matplotlib inline 
def graph_xva(t, x, v, a,x_label = 'x (m)', v_label='v (m/s)', a_label='a (m/s^s)'):
    '''Graph position, velocity, and acceleration against time.'''
    import matplotlib
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    ax.set_title('Position (x), Velocity (v), and Acceleration (a) in SHM')
    ax.plot(t,x,label=x_label)
    ax.plot(t,v,label=v_label)
    ax.plot(t,a,label=a_label)
    ax.minorticks_on()
    ax.legend(loc='upper right')
    ax.grid(which="both")
    ax.axhline(y=0, color='k')
    plt.show()
# +
import ipywidgets as widgets
import numpy as np
pi = np.pi
def graph_shm(w=1.0,A=1.1,phi=0):
    from numpy import sin, cos
    t = np.linspace(0,10,200)
    x = A*cos(w*t+phi)
    v = -A*w*sin(w*t+phi)
    a = -A*w*w*cos(w*t+phi)
    graph_xva(t,x,v,a)

widgets.interact(w=(0.5,5.0),phi=(-pi,pi),A=(0,5.0))(graph_shm)


# -

graph_shm(**shm_answer)


graph_shm(**shm_answer2)

    
