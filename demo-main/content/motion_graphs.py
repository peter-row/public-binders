# Cathy Freeman 400m - https://youtu.be/KOVhDUkmIvk?t=38

# Ideas: 🪂 🏃🏾‍♀️🚀
#
# Description, then prediction.
#
# 1. Diagram of runner. 🏃🏾‍♀️ with "motion diagram" -> graphs
#
# 2. Diagram of a rocket with "motion diagram" -> graphs
#
# 3. Diagram of a parachute with "motion diagram" -> graphs
#
# 4. Diagram of a rocket then parachute with "motion diagram" -> graphs
#

try:
    import micropip
    await micropip.install("ipywidgets")
    await micropip.install("bqplot")
except:
    pass

# +
# sketch pad

import ipywidgets as widgets
import numpy as np
import bqplot.pyplot as plt
import time

# state:
# - start drawing
# - end drawing
def make_sketchpad(xlim, ylim,  xlabel = 'X Position, x / m', ylabel = 'Y Position, y / m'):
    fig = plt.figure()
    axes_options = {'x':{'label':xlabel}, 'y':{'label':ylabel}}
    #plt.label(['📹','motion sensor'],x=[7,7],y=[0,-1])
    delta_x_labels = plt.scatter([],[],default_size=0)
    delta_x_graph = plt.plot([],[],colors=['black'])
    motion_graph = plt.scatter([],[], default_size=64)
    sketchpad = plt.scatter([0.0],[0.0],default_size=128, colors = ['red'],
                            axes_options=axes_options,enable_move=True)
    state_obj = ['init'] # ugly hack so I can modify "state" inside the callback function
    path = {'x':[],'y':[],'t0':None,'t':[]}
    path_graph = plt.plot(x=path['x'],y=path['y'])
    axis_graph = plt.plot([],[],line_style ='dashed')
    callbacks = []
    def drag_callback(name, value):
        with sketchpad.hold_sync():
            state = state_obj[0]
            xi = value['point']['x']
            yi = value['point']['y']
            t = time.time()
            if state=='init':
                path['x']=[xi]
                path['y']=[yi]
                path['t0']=t
                path['t']=[0.0]
                state_obj[0]='drawing'
                motion_graph.x = []
                motion_graph.y = []
                motion_graph.names = []
            else:
                path['x'].append(xi)
                path['y'].append(yi)
                path['t'].append(t-path['t0'])
            if value['event']=='drag_end':
                state_obj[0] = 'init'
                time_points = np.arange(0,path['t'][-1],0.2)
                x_points = np.interp(time_points,path['t'],path['x'])
                y_points = np.interp(time_points,path['t'],path['y'])
                path['t_points'] = time_points
                path['x_points'] = x_points
                path['y_points'] = y_points
                axis_direction = np.arctan2(y_points[-1]-y_points[0],x_points[-1]-x_points[0])
                def transform(x,y):
                    x0 = x_points[0]
                    y0 = y_points[0]
                    theta = np.arctan2(y-y0,x-x0)
                    x_transformed = (x-x0)*np.cos(theta) + (y-y0)*np.sin(theta)
                    y_transformed = (x-x0)*np.sin(theta) - (y-y0)*np.cos(theta)
                    return x_transformed,y_transformed
                
                path['s_points'] = transform(x_points,y_points)[0]
                path['s'] = transform(np.asarray(path['x']),np.asarray(path['y']))[0]
                motion_graph.x = x_points
                motion_graph.y = y_points
                motion_graph.names = ['t=%.2f'%t for t in time_points]
                delta_x_graph.x = [[x+50*np.sin(axis_direction),x-50*np.sin(axis_direction)] for x in x_points]
                delta_x_graph.y = [[y-50*np.cos(axis_direction),y+50*np.cos(axis_direction)] for y in y_points]
                s_points = [(s1+s2)/2.0 for (s1,s2) in  zip(path['s_points'][:-1],path['s_points'][1:])]
                delta_x_labels.x =[-np.sin(axis_direction)+np.cos(axis_direction)*(s)+x_points[0] for s in s_points]#(x_points[1:]+x_points[:-1])/2.0
                delta_x_labels.y = [np.cos(axis_direction)+np.sin(axis_direction)*(s)+y_points[0] for s in s_points]#ylim[1]-1+0*x_points[1:]
                delta_x_labels.names = ['Δs' for x in x_points[1:]]
                # axis direction
                axis_graph.x = [x_points[0],x_points[0]+np.cos(axis_direction)*50]
                axis_graph.y = [y_points[0],y_points[0]+np.sin(axis_direction)*50]
                for callback in callbacks:
                    callback(path)
            path_graph.x = path['x']
            path_graph.y = path['y']
            sketchpad.x[0] = xi
            sketchpad.y[0] = yi
    sketchpad.on_drag(drag_callback)
    sketchpad.on_drag_end(drag_callback)
    plt.xlim(*xlim)
    plt.ylim(*ylim)
    return fig, callbacks
    
fig, callbacks = make_sketchpad((-10,10),(-5,5))
fig

# +
fig = plt.figure()
def plot_s_graph(path):
    # make "patches" to show the gradients
    s_graph.x = path['t_points']
    s_graph.y = path['s_points']
    s_all_graph.x = path['t']
    s_all_graph.y = path['s']
    ds_graph.x = [[t1,t2,t2,t1] for (t1,t2) in zip(path['t_points'][:-1],path['t_points'][1:])]
    ds_graph.y = [[x1,x2,x1,x1] for (x1,x2) in zip(path['s_points'][:-1],path['s_points'][1:])]
    ds_graph.fill_colors = ['lightgray' for p in path['t_points'][:-1]]
    #ds_labels.x = d

ds_graph = plt.plot([[1,2,1]],[[1,2,1]],
                    #labels=['label'],#['̅vₐᵥ = Δx/Δt'],
                    stroke_width=0,
                    fill_colors=["lightgray"],fill='inside')
s_graph = plt.scatter([],[])
s_all_graph = plt.plot([],[])
#ds_labels = plt.scatter([],[],default_size=0)
callbacks.append(plot_s_graph)

fig

# +
fig = plt.figure()
def plot_v_graph(path):
    # make "patches" to show the gradients
    average_v = (path['x_points'][:-1]-path['x_points'][1:])/(path['t_points'][:-1]-path['t_points'][1:])
    #v_graph.x = (path['t_points'][:-1]+path['t_points'][1:])/2.0
    #v_graph.y = average_v
    v_bars.x = [[t1,t2,t2,t1,t1] for (t1,t2) in zip(path['t_points'][:-1],path['t_points'][1:])]
    v_bars.y = [[0,0,v,v,0] for v in average_v]
    v_bars.fill_colors = ['lightgray' for p in v_bars.y]
    v_bars.colors=['black' for x in v_bars.x]
v_bars = plt.plot([],[],stroke_width=1,fill_colors=["lightgray"],fill='inside')
#v_graph = plt.scatter([],[])
callbacks.append(plot_v_graph)

fig
# -

v_bars.y



def plot_distance(path):
    x = np.asarray(path['x_points'])
    y = np.asarray(path['y_points'])
    # Pythagorous
    distance = (((x[1:]-x[:-1])**2 + (y[1:]-y[:-1])**2)**0.5).cumsum()
    t = path['t_points']
    distance_plot.x = t[1:]
    distance_plot.y = distance
    # repeat for line plot:
    x = np.asarray(path['x'])
    y = np.asarray(path['y'])
    # Pythagorous
    distance = (((x[1:]-x[:-1])**2 + (y[1:]-y[:-1])**2)**0.5).cumsum()
    distance_lines.x = path['t'][1:]
    distance_lines.y = distance
callbacks.append(plot_distance)

# +
#import micropip
#await micropip.install("ipywidgets")

import numpy as np
runner_x = [0.0,0.3,0.8,1.5,2.3,3.4,4.5,5.6,6.7,7.8,9.0]
runner_t = [0,1,2,3,4,5,6,7,8,9,10]

import ipywidgets as widgets
import bqplot.pyplot as plt

plt.figure(2, title="Diagram of a Runner's Positions")
scatter = plt.scatter(runner_x,[1]*len(runner_x), enable_move=True)
plt.xlabel("Position, x / m")
plt.show()

def scatter_cb(name,value):  
        if value['event'] in ('drag','drag_end'):
            with scatter.hold_sync():
                i = value['index']
                scatter.x[i]=value['point']['x']

scatter.on_drag(scatter_cb)
scatter.on_drag_end(scatter_cb)



# +
import numpy as np
from bqplot import Graph, LinearScale, ColorScale, Figure, Tooltip
from ipywidgets import Layout

def plot_v_graph(path):
    node_data = [str(i) for i in path['x_points']]
    link_data = [{'source': i, 'target': i+1} for i in path['x_points']]
    vgraph.node_data = node_data
    vgraph.link_data = link_data
    vgraph.x = path['x_points']
    vgraph.y = path['y_points']

xs = LinearScale()
ys = LinearScale()
x = runner_x
y = np.ones(len(runner_x))
vgraph = Graph(node_data=[], link_data=[], link_type='line',
              scales={'x': xs, 'y': ys, }, x=[], y=[], colors=['orange'])

callbacks.append(plot_v_graph)

Figure(marks=[vgraph])
# -

print(graph.x)

# ## Teacher notes
#
# see "Five Easy Lessons" by Knight
#
# * Students want to "jump to the math" instead of understanding the question / problem solving
#
# * Students often have a persistant undifferentiated conceptual view of "motion", rather than breaking it up into displacement, velocity, and acceleration; even at a university level.
#
# * Position and velocity are confused (with students mistakenl thinking the "winning" car is faster).
#
# * Acceleration and velocity are frequently confused (e.g. at the top of motion under gravity, students will think a=0 instead of a=-g).
#
# * Students will mistake "a vs b" graphs and "b vs a"
#
# * gradient is not x/y, it's dx/dy
#
# * gradients have units
#
# * "area under a curve" is an odd concept, and units are tricky.
#
# * Average vs instantaneous is tricky



