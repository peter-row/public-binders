# Gravity
# =====
#  

from IPython.display import YouTubeVideo
YouTubeVideo('CDAKa1gOc_c')

# The force of gravity on a 1kg mass, from a 20kg mass, is given by: 
#
# $$F_g = \frac{GMm}{r^2}$$
#
# M = 20kg
#
# m = 1kg
#
# r = distance between the centres of the masses
#
# G is the Gravitational constant

# Graph of Fg vs r relationship
import numpy as np
import matplotlib.pyplot as plt
G = 6.67430*10**-11
def gravity(M,m,r,r_M,hollow=True):
    if r_M>0:
        interior = r<r_M
        F = r*0.0
        if hollow:
            F[interior] = 0.0
        else:
            F[interior]=(G*M*m/(r_M*r_M))*(r[interior]/r_M)
        F[~interior] = G*M*m/((r*r)[~interior])
    else:
        F = G*M*m/(r*r)
    return F
M = 20.0
m = 1.0
r = 0.1
F = gravity(M,m,r,0)
print('F=%.1eN'%F)


from ipywidgets import interact
@interact(M=(0,100.0),m=(0,20.0),r=(0,0.5,0.01))
def g(M,m,r):
    F = gravity(M,m,r,0)
    return 'F=%.1eN'%(F)


# +
Me = 5.972* (10**24) # mass of Earth, kg
Re = 6371*1000 # Earth radius

r = np.linspace(0,Re*3,500)
F = gravity(Me,1.0,r,Re)
plt.plot(r,F) 
plt.xlabel('Distance, r / m')
plt.ylabel('Force of gravity, F / N')
plt.show()
# -

print(F.max())

# ### import matplotlib.pyplot as plt
# import numpy as np
#
# r = np.linspace(0.1,0.3)
# # make radius values from 0.1 to 0.3
#
# M = 20.0
# m = 1.0
# F = gravity(80.0,1.0,r)
#
# plt.plot(r,F*10**6) # multiply F by 1 million
# plt.xlabel('Distance, r / m')
# plt.ylabel('Force of gravity, F / µN')
# plt.show()

# +
Me = 5.972* (10**24) # mass of Earth, kg
Re = 6371*1000 # Earth radius
Mm = 7.35 * 10**22 # mass of Moon, kg
Dm = 384400*1000 # Earth-moon distance, m
Rm = 1737*1000 # Moon radius
x = np.linspace(0,Dm,5000)
# a list of x values between the Earth and Moon

r_earth = x # distance from the x to the Earth's centre
r_moon = Dm-x # distance from x to the moon's centre
F_earth = gravity(Me,1.0,x,Re)
F_moon = gravity(Mm,1.0,r_moon,Rm)
plt.plot(x,F_earth-F_moon) 
plt.xlabel('Distance, r / m')
plt.ylabel('Force of gravity, F / N')
plt.grid()
plt.show()


# -

def clipped_quiver(X,Y,U,V,q=0.99):
    import matplotlib.cm as cm
    from matplotlib.colors import Normalize
    colormap = cm.viridis
    Z = ((U**2)+(V**2))**0.5
    top = np.quantile(Z,q)
    top_mask = (Z>=top) 
    Z_clipped = np.clip(Z,0,top)
    U_clipped = U[:]
    V_clipped = V[:]
    U_clipped[top_mask] = U[top_mask]*Z_clipped[top_mask]/Z[top_mask]
    V_clipped[top_mask] = V[top_mask]*Z_clipped[top_mask]/Z[top_mask]
    norm = Normalize()
    norm.autoscale(Z_clipped)
    colors = colormap(norm(Z_clipped.flatten()))
    plt.quiver(X, Y, U_clipped, V_clipped, pivot='tip', color=colors)


# +
def gravity2D(m,M,x,y,x0,y0,r_M, hollow=True):
    '''Force of gravity on m at point x,y from M at point x0,y0'''
    r_squared = (x-x0)**2.0 + (y-y0)**2 # Pythagorous
    r_cubed = r_squared**(3.0/2.0)
    if r_M>0:
        interior = r_squared < r_M**2
        Fx = x*0.0
        Fx[interior]=(G*M*m/(r_M*r_M))*((x0-x)[interior]/r_M)
        Fx[~interior] = G*M*m*(x0-x)[~interior]/(r_cubed[~interior])
        Fy = x*0.0
        Fy[interior]=(G*M*m/(r_M*r_M))*((y0-y)[interior]/r_M)
        Fy[~interior] = G*M*m*(y0-y)[~interior]/(r_cubed[~interior])
        if hollow:
            Fx[interior] = 0.0
            Fy[interior] = 0.0
    else:
        Fx = G*m*M*(x0-x)/r_cubed
        Fy = G*m*M*(y0-y)/r_cubed # displacement / r^3
    return Fx,Fy

x = np.linspace(-3*Re, 3*Re, 30)
y = np.linspace(-3*Re, 3*Re, 30)
X, Y = np.meshgrid(x, y)
mask = (X**2+Y**2)>(0)**2
U, V = gravity2D(1.0,Me,X,Y,0.0,0.0,Re)
clipped_quiver(X,Y,U,V,q=0.9)
#plt.quiver(X, Y, U, V)
plt.show()
# -

plt.streamplot(X, Y, U, V, density=.2,broken_streamlines=False)
plt.show()

# +

x = np.linspace(-Dm/2.0, 1.5*Dm, 30)
y = np.linspace(-Dm, Dm, 30)
X, Y = np.meshgrid(x, y)
mask = (X**2+Y**2)>(Re)**2
mask = mask & (((X-Dm)**2 + (Y)**2)>((Rm)**2))
U_earth, V_earth = gravity2D(1.0,Me,X,Y,0.0,0.0,Re)
U_moon, V_moon = gravity2D(1.0,Mm,X,Y,Dm,0.0,Rm)

U = U_earth+U_moon
V = V_earth+V_moon

clipped_quiver(X,Y,U,V,q=0.9)
plt.show()

# -

plt.streamplot(X, Y, U, V, density=.3,broken_streamlines=False)
plt.show()

# +
x = np.linspace(-Dm/2.0, 1.5*Dm, 1000)
y = np.linspace(-Dm, Dm, 1000)
X, Y = np.meshgrid(x, y)
mask = (X**2+Y**2)>(Re)**2
mask = mask & (((X-Dm)**2 + (Y)**2)>((Rm)**2))
U_earth, V_earth = gravity2D(1.0,Me,X,Y,0.0,0.0,Re)
U_moon, V_moon = gravity2D(1.0,Mm,X,Y,Dm,0.0,Rm)

U = U_earth+U_moon
V = V_earth+V_moon

plt.streamplot(X, Y, U, V, density=.75,broken_streamlines=False)
plt.show()


# +
def GPE_2D(m, M, x,y,x0,y0, r_M, hollow=True):
    '''Potential energy of m at point x,y from M at point x0,y0'''
    r_squared = (x-x0)**2.0 + (y-y0)**2 # Pythagorous
    r = r_squared**0.5
    if r_M == 0:
        return -G*m*M/r
    # not in course, some formula
    interior = r<r_M
    V = x*0
    V[~interior] = -G*m*M/r[~interior]
    if hollow:
        V[interior] = -G*m*M/r_M
    else:
        V[interior] = G*m*M/(2*r_M**3)*(r_squared[interior]-3*r_M**2)
    return V
    
x = np.linspace(-5*Re, 5*Re, 500)
y = np.linspace(-5*Re, 5*Re, 500)
X, Y = np.meshgrid(x, y)
mask = (X**2+Y**2)>(Re)**2
U = GPE_2D(1.0,Me,X,Y,0.0,0.0,Re)
#U[~mask]=U[mask].min()

plt.plot(X[250],U[250])
plt.show()

# +

x = np.linspace(-5*Re, 5*Re, 500)
y = np.linspace(-5*Re, 5*Re, 500)
X, Y = np.meshgrid(x, y)
U = GPE_2D(1.0,Me,X,Y,0.0,0.0,Re)

ax = plt.figure().add_subplot(projection='3d')
ax.plot_surface(X, Y, U, edgecolor='royalblue', lw=0.1, rstride=8, cstride=8,
                alpha=0.1)

#ax.contour(X, Y, U, 20, cmap=cm.viridis)  # Plot contour curves
#ax.contour(X, Y, U, 20, zdir='z', offset=U.min(), cmap='coolwarm')
plt.show()

# +
ax = plt.figure().add_subplot(projection='3d')
ax.plot_surface(X, Y, U, edgecolor='royalblue', lw=0.05,rstride=8, cstride=8,
                alpha=0.1)

ax.contour(X, Y, U, 20, cmap="viridis")  # Plot contour curves
#ax.contour(X, Y, U, 20, zdir='z', offset=U.min(), cmap='coolwarm')
plt.show()

# +
ax = plt.figure().add_subplot(projection='3d')
ax.plot_surface(X, Y, U, edgecolor='royalblue', lw=0.1, rstride=8, cstride=8,
                alpha=0.1)

#ax.contour(X, Y, U, 20, cmap=cm.viridis)  # Plot contour curves
ax.contour(X, Y, U, 20, zdir='z', offset=U.min(), cmap='viridis')
plt.show()
# -

ax = plt.figure().add_subplot()
ax.contour(X, Y, U, 20, cmap='viridis')
plt.show()

# +
ax = plt.figure().add_subplot()
ax.contour(X, Y, U, 20, cmap='viridis')
x = np.linspace(-5*Re, 5*Re, 20)
y = np.linspace(-5*Re, 5*Re, 20)
Xg, Yg = np.meshgrid(x, y)
Ug, Vg = gravity2D(1.0,Me,Xg,Yg,0.0,0.0,Re)
clipped_quiver(Xg,Yg,Ug,Vg,q=0.9)

plt.show()
# -

ax = plt.figure().add_subplot()
plt.streamplot(Xg, Yg, Ug, Vg, density=.3,broken_streamlines=False)
ax.contour(X, Y, U, 20, cmap='viridis')
plt.show()

# +

x = np.linspace(-Dm/2.0, 1.5*Dm, 100)
y = np.linspace(-Dm, Dm, 100)
X, Y = np.meshgrid(x, y)
mask = (X**2+Y**2)>(Re)**2
mask = mask & (((X-Dm)**2 + (Y)**2)>((Rm)**2))
U_earth, V_earth = gravity2D(1.0,Me,X,Y,0.0,0.0,Re)
U_moon, V_moon = gravity2D(1.0,Mm,X,Y,Dm,0.0,Rm)

U = U_earth+U_moon
V = V_earth+V_moon
GPE_earth = GPE_2D(1.0,Me,X,Y,0.0,0.0,Re)
GPE_moon = GPE_2D(1.0,Mm,X,Y,Dm,0.0,Rm)
GPE = GPE_earth+GPE_moon
#GPE[~mask]=GPE[mask].min()

ax = plt.figure().add_subplot(projection='3d')
ax.plot_surface(X, Y, GPE, edgecolor='royalblue', lw=0.1, rstride=8, cstride=8,
                alpha=0.1)

#ax.contour(X, Y, U, 20, cmap=cm.viridis)  # Plot contour curves
ax.contour(X, Y, GPE, 50, zdir='z', offset=GPE.min(), cmap='viridis')
plt.show()

# +
ax = plt.figure().add_subplot()

plt.streamplot(X, Y, U, V, density=.75,broken_streamlines=False)
ax.contour(X, Y, GPE, 100, cmap='viridis')
plt.show()


# +

x = np.linspace(-2*Dm,2*Dm, 100)
y = np.linspace(-2*Dm, 2*Dm, 100)
X, Y = np.meshgrid(x, y)
mask = (X**2+Y**2)>(Re)**2
mask = mask & (((X-Dm)**2 + (Y)**2)>((Rm)**2))
U_earth, V_earth = gravity2D(1.0,Me,X,Y,0.0,0.0,Re)
U_moon, V_moon = gravity2D(1.0,Mm,X,Y,Dm,0.0,Rm)

U = U_earth+U_moon
V = V_earth+V_moon
GPE_earth = GPE_2D(1.0,Me,X,Y,0.0,0.0,Re)
GPE_moon = GPE_2D(1.0,Mm,X,Y,Dm,0.0,Rm)
GPE = GPE_earth+GPE_moon
GPE[~mask]=GPE[mask].min()

ax = plt.figure().add_subplot()

plt.streamplot(X, Y, U, V, density=.75,broken_streamlines=False)
ax.contour(X, Y, GPE, 80, cmap='viridis')
plt.show()
# -




