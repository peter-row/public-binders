# -*- coding: utf-8 -*-
# Note - to create all the graphs, click "⏩️" or "Kernel" - "Restart and Run All Cells".
#
# I am using Jupyter to display this as an online Python notebook. I have also added the jupytext extension, 
# which allows me to use files with the standard ".py" files (instead of the ".ipynb" files which Jupyter uses).

# # Hooke's Law
#
# ## Equilibrium position
#
# A spring in equlibrium position has $F_r=0$

# +
# a "magic" command to show graphs in Jupyter:
# %matplotlib inline 

# I wrote the code for graphing in a different file, so I need to import it.
from HookesLawGraphing import plot_cart
# call the function I imported, with arguments "x=0" and "F=0"
plot_cart(x=0,F=0)
# -

# ## Compression
#
# A spring that is compressed (so $x$, the displacement from equilibrium, is negative) has a **restoring force** by the spring of $F_r > 0$ - the spring will push the cart in the positive direction.

x = -1.0
F = -1.5*x # "*"" means "times" in Python
plot_cart(x=x,F=F)

# ## Extension
#
# A spring that is extended (so $x$, the displacement from equilibrium, is positive) has a **restoring force** by the spring of $F_r < 0$ - the spring will pull the cart in the negative direction.
#
# **Try moving the slider**

# +
# import a function to show interactive "widgets"
import ipywidgets as widgets
# functions to display HTML (converted from Markdown)
from IPython.display import display, Markdown

@widgets.interact(x=(-2.0,2.0))
def show_cart(x=1.0):
    k = 1.5
    F = -k*x # Hooke's Law
    PE = 0.5*k*x**2 # ** is 'power' - calculate potential energy
    plot_cart(x,F,PE)
    # logical statement - display a different text depending on x
    if x==0:
        display(Markdown("# Equilibrium Position: Restoring Force = 0"))
    if x>0:
        display(Markdown("# Extension: Restoring Force <0"))
    if x<0:
        display(Markdown("# Compression: Restoring Force >0"))


# -

# ## Hooke's Law
#
# Hooke's law is given by $F_r = -kx$ where $k$ is the **spring constant**, $x$ is the displacement from equilibrium, and $F_r$ is the restoring force by the spring.
#
# If you plot the force on the spring ($F_{applied} = -F_r$) against displacement ($x$) then $k$ will be the gradient.

from HookesLawGraphing import plot_hookes_law
import numpy as np
@widgets.interact(k=(0,2.0))
def show_hookes_law(k=1.0):
    # np is a scientific / mathematical extension to python.
    # linspace will create an array (similar to a list) of values
    x = np.linspace(0,0.5,21) # 21 points evenly spaced between 0 and 0.5
    Fr = -k*x # this will be an array of forces
    F = -Fr # applied force
    # plot it
    plot_hookes_law(x=x, F=F)


# ## Hookean and non-Hookean Springs
#
# A spring is "Hookean" if it follows Hooke's Law. In reality, a spring can only be Hookean over a certain range (it will eventually begin to deform, then eventually break).

# +
# the code here is complicated, and not very useful.

@widgets.interact(k=(0,2.0))
def show_hookes_law(k=1.0):
    x = np.linspace(0,0.5,31)
    # some complicated relationship between x and Fr
    # if x<0.5, spring is normal
    # if 0.5<x<0.7 then spring is 'stretched out' and gradient will increase
    # if 0.7<x<1 then it starts to elastically deform, and gradient is slightly negative
    # if 1<x10.1 then it is breaking, and k is very negative
    x1 = 0.2
    x2 = 0.25
    x3 = 0.35
    x4 = 0.45
    x5 = 0.48
    # interpolate:
    Fr = np.interp(x,[0,x1,x2,x3,x4,x5],[0,-k*x1,-0.45*k,-0.43*k,0.02,0],right=0)
    F = -Fr # 
    plot_hookes_law(x=x, F=F)


# -

# ## Simulating a cart
#
# To use the built-in simulation code, we need a function that calculates
# "derivatives" (velocity and accelleration) given the current state (position
# and velocity).
#
# Velocity is trivial, since it is already part of the current state.

import HookesLawProblems
HookesLawProblems.derive_a()


# +
def derivatives(state,t):
    x, v = state
    F = -k * x
    a = F/m
    return (v, a)

def simulate_cart(x0, v0, m, k, max_t, dt):
    from scipy import integrate
    t = np.linspace(0,max_t,int(max_t/dt))
    # numerically integrate.
    result = integrate.odeint(derivatives, (x0,v0), t)
    x = result[:,0]
    v = result[:,1]
    return x,v

from HookesLawGraphing import animate_cart
k=1.3
m=0.5
x_max=0.6
v0 = 0.0
max_t = 2*2*np.pi*(m/k)**0.5 # how long we will simulate
dt = 0.1
x_series, v_series = simulate_cart(x_max,v0, m,k,max_t,dt)
animate_cart(x_series, v_series, max_t, dt)
# -

HookesLawProblems.max_v()

# ## Energy in a Hookean Spring
#
# The potential energy in a Hookean Spring is given by $U_{P} = \frac{1}{2} k x^2$ - the area under the triangle on the displacement-force graph.
#
# The kinetic energy of the moving mass is given by $U_{K} = \frac{1}{2} m v^2$.

HookesLawProblems.max_v_x_questions()
HookesLawProblems.energy_total()


# If energy is constant, and the spring is left to oscilate, conservation of energy would mean $U = \frac{1}{2} k x^2 + \frac{1}{2}mv^2$, where $m$ and $v$ are the mass and velocity of the mass attached to the end.

HookesLawProblems.x_from_max_v()

# +
from HookesLawGraphing import plot_displacement_vs_v

@widgets.interact(k=(0.1,2.0),m=(0.1,2.0),x_max=(-1.0,1.0))
def show_displacement_vs_v(k=1.3,m=0.5,x_max=0.6):
    # E is constant
    E = 0.5*k*x_max**2 # energy with maximum displacement
    
    # get x points, evenly spaced on the x axis
    x = np.linspace(-x_max,x_max,20)
    KE = 0.5*k*x**2
    PE = E - KE
    v = (2*PE/m)**0.5
    plot_displacement_vs_v(x=x,v=v)


# -

from HookesLawGraphing import animate_displacement_vs_v
max_t = 2*2*np.pi*(0.4/0.8)**0.5
dt = 0.1
x_series_list = []
v_series_list = []
param_list = []
(k,m,x_max) = (0.8,0.4,0.6)
param_list.append('k=%s, m=%s'%(k,m))
x_series, v_series = simulate_cart(x_max,0,m,k,max_t, dt)
x_series_list.append(x_series)
v_series_list.append(v_series)
#animate_displacement_vs_v(x_series_list,v_series_list,param_list,dt)

# +
x_series_list = []
v_series_list = []
param_list = []

for (k,m,x_max) in ((0.8,0.4,0.6),
                    (0.8,0.4,0.3),
                    (0.8,0.4,0.1)):
    x_series, v_series = simulate_cart(x_max,0,m,k, max_t, dt)
    x_series_list.append(x_series)
    v_series_list.append(v_series)
    param_list.append('k=%s, m=%s'%(k,m))

#animate_displacement_vs_v(x_series_list,v_series_list,param_list,dt)

# +
x_series_list = []
v_series_list = []
param_list = []

for (k,m,x_max) in ((0.8,0.4,0.3),
                    (0.2,0.4,0.3),
                    (0.8,0.1,0.3)):
    x_series, v_series = simulate_cart(x_max,0,m,k, max_t, dt)
    x_series_list.append(x_series)
    v_series_list.append(v_series)
    param_list.append('k=%s, m=%s'%(k,m))

#animate_displacement_vs_v(x_series_list,v_series_list,param_list,dt)

# +
x_series_list = []
v_series_list = []
param_list = []

for (k,m,x_max) in ((0.8,0.4,0.6),
                    (0.2,0.4,0.8),
                    (0.8,0.1,0.3)):
    x_series, v_series = simulate_cart(x_max,0,m,k, max_t, dt)
    x_series_list.append(x_series)
    v_series_list.append(v_series)
    param_list.append('k=%s, m=%s'%(k,m))

#animate_displacement_vs_v(x_series_list,v_series_list,param_list,dt)
# -


