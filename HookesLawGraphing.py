# +
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

def spring_points(x1, y1, x2, y2, width, n):
    '''Plot of a spring from (x1,y1) to (x2,y2) with n periods, it will have 4n+1 points'''
    # Pythag
    Length = ((x2-x1)**2 + (y2-y1)**2)**0.5
    # create a sawtooth pattern with n periods from (0,0) to (Length,0)
    x_ = np.linspace(0,1,4*n+1) * Length
    y_ = np.asarray([0.0,0.5,0.0,-0.5]*n+[0])*width
    # find angle (with defaults if length is zero)
    cosa = 1.0
    sina = 0.0
    if Length>0:
        cosa = (x2-x1)/Length
        sina = (y2-y1)/Length
    # rotate sawtooth and add to origin
    x = x1 + (x_*cosa - y_*sina)
    y = y1 + (x_*sina + y_*cosa)
    return x,y

def plot_cart(x,F,PE=None):
    x0 = 0 # equilibrium position
    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    # plot the spring
    ax.plot(*spring_points(-3,0,x0+x,0,0.7,5))
    # plot equilibrium point and x
    ax.plot([0,0],[3,-3],linestyle='dashed')
    ax.plot([x,x],[3,-3],linestyle='dashed')
    ax.plot([-5,5],[-0.5,-0.5],linestyle='solid',color='k')
    # scale
    ax.set_xlim((-3,3))
    ax.set_ylim((-3,3))
    ax.get_yaxis().set_visible(False)    
    # labels
    ax.set_xlabel('Displacement from equilibrium, x/m')
    # add arrows for force
    ax.add_patch(mpatches.FancyArrowPatch((x0,2),(x0+x,2),mutation_scale=20))
    ax.text(x0+x/2.0,2.5, "x=%.2fm"%x,size=20)
    ax.add_patch(mpatches.FancyArrowPatch((x0+x,-2),(x0+x+F,-2),mutation_scale=20))
    ax.text(x0+x/2.0,-2.5, "F=%.2fN"%F,size=20)
    # draw the cart
    ax.add_patch(mpatches.Rectangle((x0+x,-0.3), 1,1,color='y'))
    ax.add_patch(mpatches.Circle((x0+x+.2,-.4), .1,color='k'))
    ax.add_patch(mpatches.Circle((x0+x+0.8,-.4), .1,color='k'))
    if PE is not None:
        ax.text(-2,-1.5, "PE=%.2f J"%PE,size=20)
    #ax.grid(True)
    plt.show()



# -

def plot_hookes_law(F, x):
    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    ax.scatter(x,F)
    trend = np.polyfit(x[:5], F[:5],3)
    ax.plot(x,np.poly1d(trend)(x), linestyle='dashed') #plot trend line
    ax.set_xlabel(r'Displacement, $x$ / m', fontsize=15)
    ax.set_ylabel(r'Force on the spring, $F$ / N', fontsize=15)
    ax.set_title('Displacement of a spring vs force on the spring')
    ax.set_xlim(0,0.5)
    ax.set_ylim(0,.5)
    ax.grid(True)
    plt.show()


def plot_displacement_vs_v(x,v):
    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    ax.scatter(x,v,color='b')
    ax.scatter(x,-v,color='b')
    ax.set_aspect('equal')
    # axis lines
    ax.axhline(y=0, color='k')
    ax.axvline(x=0, color='k')
    # label
    ax.set_xlabel(r'Displacement, $x$ / m', fontsize=15)
    ax.set_ylabel(r'Velocity of mass, $v$ / $ms^{-1}$', fontsize=15)    
    ax.set_xlim(-1,1)
    ax.set_ylim(-1,1)
    ax.grid(True)
    plt.show()



def animate_cart(x_series, v_series, max_t, dt):
    from matplotlib import animation
    # set up the graph
    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    # equilibrium point
    ax.plot([0,0],[3,-3],linestyle='dashed')
    ax.plot([-5,5],[-0.5,-0.5],linestyle='solid',color='k')
    # scale
    ax.set_xlim((-3,3))
    ax.set_ylim((-3,3))
    ax.get_yaxis().set_visible(False)    
    # labels
    ax.set_xlabel('Displacement from equilibrium, x/m')
    x0 = 0.0
    # initialise lines
    spring_line, = ax.plot(*spring_points(-3,0,x0+0,0,0.7,5))
    x_line, = ax.plot([0,0],[3,-3],linestyle='dashed')
    x_annotation = ax.annotate('x = %.2f'%x_series[0], xy=(x_series[0],2), xytext=(0,1.85),
                    arrowprops = {'width':2.0}, fontsize=15)
    v_annotation = ax.annotate('v = %.2f'%v_series[0], xy=(x_series[0]+v_series[0],-2), xytext=(x_series[0],-2.15),
                    arrowprops = {'width':2.0}, fontsize=15)
    def animate(i):
        x = x_series[i]
        v = v_series[i]
        
        # change lines
        spring_line.set_data(*spring_points(-3,0,x0+x,0,0.7,5))
        x_line.set_data((x,x),(3,-3))
        
        # clear drawings
        ax.patches=[]
        # draw arrows for displacement and v:
        #x_arrow = ax.add_patch(mpatches.FancyArrowPatch((x0,2),(x0+x,2),mutation_scale=20))
        #v_arrow = ax.add_patch(mpatches.FancyArrowPatch((x0+x,-2),(x0+x+v,-2),mutation_scale=20))
        
        #v_annotate = ax.annotate('v = %s'%v, xy=(x+v,-2), xytext=(x,-2))
        #x_annotation.set_position((0,2))
        x_annotation.xy = (x,2)
        x_annotation.set_text('x')# = %.2f'%x)
        v_annotation.xy = (x+v,-2)
        v_annotation.set_position((x,-2.15))
        v_annotation.set_text('v')# = %.2f'%v)
        # draw the cart
        cart = ax.add_patch(mpatches.Rectangle((x0+x,-0.3), 1,1,color='y',animated=True))
        wheel1 = ax.add_patch(mpatches.Circle((x0+x+.2,-.4), .1,color='k',animated=True))
        wheel2 = ax.add_patch(mpatches.Circle((x0+x+0.8,-.4), .1,color='k',animated=True))
        return spring_line, x_line, x_annotation,v_annotation
    ani = animation.FuncAnimation(
        fig, animate, len(x_series), interval=dt*1000, blit=True)
    from IPython.display import HTML
    display(HTML(ani.to_jshtml()))
    fig.clear()


def animate_displacement_vs_v(x_series_list,v_series_list,param_list,dt):
    from collections import deque
    from matplotlib import animation
    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    # axis lines
    ax.axhline(y=0, color='k')
    ax.axvline(x=0, color='k')
    # label
    ax.set_xlabel(r'Displacement, $x$ / m', fontsize=15)
    ax.set_ylabel(r'Velocity of mass, $v$ / $ms^{-1}$', fontsize=15)    
    ax.set_xlim(-1,1)
    ax.set_ylim(-1,1)
    ax.grid(True)
    # lines to animate:
    lines = [ax.plot([], [], 'o-', lw=2)[0] for xs in x_series_list]
    traces = [ax.plot([], [], ',-', lw=1)[0] for xs in x_series_list]
    annotations = [ax.annotate(t, xy=(1,0), xytext=(-1,0), fontsize=15,
                    arrowprops = {'arrowstyle': "->"}) for t in param_list]
    #labels = [ax.text(0,0,t) for t in param_list]
    history_len=30
    history_x_list = [deque(maxlen=history_len) for xs in x_series_list]
    history_v_list = [deque(maxlen=history_len) for xs in x_series_list]
    def animate(i):
        for j in range(len(x_series_list)):
            x = x_series_list[j][i]
            v = v_series_list[j][i]
            lines[j].set_data([0,x],[0,v])
            annotations[j].set_position((x*1.1,v*1.1))
            annotations[j].xy = (x,v)
            traces[j].set_data(history_x_list[j],history_v_list[j])
            history_x_list[j].appendleft(x)
            history_v_list[j].appendleft(v)
        return lines + traces + annotations
    ani = animation.FuncAnimation(fig, animate, len(x_series_list[0]), interval=dt*1000)
    from IPython.display import HTML
    display(HTML(ani.to_jshtml()))
    fig.clear()


