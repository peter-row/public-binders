# +
from sympy import S

def compare_expression(response,answer):
    try:
        A = S(response)
    except:
        display('Could not parse response')
        return False
    B = S(answer) # make it symbolic
    if A==B:
        return True
    A = A.expand()
    B = B.expand()
    if A==B:
        return True
    A = A.simplify()
    B = B.simplify()
    if (A==B):
        return True
    A = A.trigsimp()
    B = B.trigsimp()
    if (A==B):
        return True
    else:
        return False


# +
from IPython.display import display, Markdown
import ipywidgets as widgets

def formula_question(left_hand_expression,question, answers, explaination):
    display(Markdown('# Question'))
    display(question)
    key = left_hand_expression+' = '
    @widgets.interact_manual(**{key:''})
    def ask_question(**kwargs):
        response = kwargs[key]
        result = False
        for answer in answers:
            if compare_expression(response,answer):
                result = True
                display(Markdown('# Correct'))
                if response.replace(' ','')!=answers[0]:
                    display(Markdown('The correct answer is:'))
                    display(answers[0])
                break
        if not result:
            display(Markdown('# Try again'))
        display(Markdown('## Explaination'))
        display(explaination)



# +
def Textbox(default,height='2em'):
    return widgets.Textarea(value=default,layout={'width':'90%','height':height})

def make_mc_builder(n=3,max_n=5):
    # save result here:
    choices = dict(*[(('choice%i'%i,'') for i in range(max_n))])
    result = dict(question='',answer='',explaination='',**choices)
    @widgets.interact(n=(1,max_n))
    def mc_builder(n=n):
        question = result['question']
        answer = result['answer']
        explaination = result['explaination']
        choices = {}
        for i in range(n):
            k = 'choice%i'%i
            choices[k] = Textbox(result[k])
        @widgets.interact(preview=True,
                          question=Textbox(question),
                          answer=Textbox(answer),
                          explaination=Textbox(explaination,height='6em'),
                          **choices)
        def preview_mc(preview,question,answer,explaination,**choices):
            result.update(dict(question=question,
                               answer=answer,
                               explaination=explaination))
            result.update(choices)
            if preview:
                display(Markdown('# Question'))
                display(Markdown(question))
#make_mc_builder()


# -

def make_match_builder(n=3,max_n=5):
    # save result here:
    labels = [('q%i'%i,'') for i in range(max_n)]
    choices = [('a%i'%i,'') for i in range(max_n)]
    print(sum(zip(labels,choices),()))
    kwargs = dict(sum(zip(labels,choices),())) # combine
    result = dict(question='',explaination='',**kwargs)
    @widgets.interact(n=(4,max_n))
    def mc_builder(n=n):
        question = result['question']
        explaination = result['explaination']
        kwargs = {}
        for i in range(n):
            for k in ('q%i'%i,"a%i"%i):
                kwargs[k] = Textbox(result[k])
        @widgets.interact(preview=True,
                          question=Textbox(question),
                          explaination=Textbox(explaination,height='6em'),
                          **kwargs)
        def preview_multi(preview,question,explaination,**choices):
            result.update(dict(question=question,
                               explaination=explaination))
            result.update(kwargs)
            if preview:
                display(Markdown('# Question'))
                display(Markdown(question))
#make_match_builder()



def mc_question(question,labels,choice_lists,answers,explaination,shuffle=True):
    if shuffle:
        import random
        choice_lists = [list(choices) for choices in choice_lists]
        [random.shuffle(choices) for choices in choice_lists]
    assert len(labels)==len(set(labels))
    assert len(labels)==len(choice_lists)
    assert len(answers)==len(answers)
    label_to_i = dict(zip(labels,range(len(labels))))
    for (answer,choices) in zip(answers,choice_lists):
        assert answer in choices
    display(Markdown('# Question'))
    display(question)
    kwargs = dict(zip(labels,choice_lists))
    @widgets.interact_manual(**kwargs)
    def ask_question(**kwargs):
        check = []
        for label in kwargs:
            i = label_to_i[label]
            if not answers[i]==kwargs[label]:
                check.append(label+' matches '+kwargs[label]+"?")
        if check:
            display(Markdown('# Check if:'))
            for l in check:
                display(l)
        else:
            display(Markdown("# Correct"))
        display(explaination)


labels = ["x = 0","x = x_max","v = 0","v = v_max"]
answers = ["v = v_max", "v = 0", "x = x_max", "x = 0"]
choices_list = [labels,labels,labels,labels]
def max_v_x_questions():
    mc_question(Markdown("Match for an elastic spring and mass:"),labels,choices_list,answers,
                Markdown("The velocity is at its maximum or minimum when x=0, and x is at "+
                        "its maximum or minimum when v=0."))


def derive_a():
    question = Markdown('How would we calculate $a$ for a spring and mass system '
                        'if we know $x$, $v$, $m$, and $k$?')
    answers = ['-k*x/m']
    explaination = Markdown('''$a = F/m$ by Newton's Second Law.
    
Where $F = -kx$ by Hooke's Law.
    ''')
    formula_question('a',question,answers,explaination)


# +
def max_v():
    question = Markdown('Where is the velocity at its minimum?')
    answers = ['0']
    explaination = Markdown('''If energy is conserved, then $U_k$ is highest when $U_P$ is zero.
    
The elastic potential energy ($U_P$) is zero at the equilibrium point.

The velocity will have its maximum and minimum values there.''')
    formula_question('x',question,answers,explaination)

def x_from_max_v():
    question = Markdown('What will the maximum $x$ be if we know the maximum velocity is $v$, in a system with '+
                       'mass $m$ and spring constant $k$?')
    answers = ['(m*v**2/k)**0.5']
    explaination = Markdown('''Equate $U_K=\\frac{1}{2} k x^2$ and $U_P=\\frac{1}{2} m v^2$.''')
    formula_question('x',question,answers,explaination)


# -

def energy_total():
    question = Markdown('''If energy is constant, and the spring is left to oscilate, 
conservation of energy would mean $U = U_k + U_P$.

Find an expression to calculate $U$, given $k$, $v$, $k$, and $m$.

Use python syntax (power is \*\* and times is \*)''')
    answers = ['1/2*k*x**2 + 1/2*m*v**2']
    explaination = Markdown("""Substitute in $U_K=\\frac{1}{2} k x^2$ and $U_P=\\frac{1}{2} m v^2$ to get 
$U = \\frac{1}{2} k x^2 +  \\frac{1}{2} m v^2$""")
    formula_question('U',question,answers,explaination)





