# %matplotlib inline
# %config InlineBackend.figure_format = 'svg'
import schemdraw
import schemdraw.elements as elm

# Key equations:
#
# ## Charge and number of electrons (or protons)
# $$Q = -Ne$$
#
# Where $Q$ is the charge in Coulomb (C), $N$ is the number of electrons minus the number of protons, and $e$ is the magnitude of the charge of an electron $e = 1.602\times10^{-19}C$.
#
# If there are more protons than electrons, the charge is positive.
#
# ## Definition of Current
# $$I_{av} = \Delta Q/\Delta t$$
# $I$ is the charge in Amps (A), $\Delta Q$ is the moving charge in Columbs (C), and $\Delta t$ is the change in time (s). A conventional current is defined to be positive in the direction of a moving positive charge, so it is in the opposite direction to moving electrons.

with schemdraw.Drawing() as d:
    d += elm.Dot(open=True).label('-0.2C')
    d += elm.Line().right().label('Wire')
    d += elm.MeterA().down().label('Ammeter')
    d += elm.Line().left().label('Wire')
    d += elm.Dot(open=True).label('+0.2C')
    d += elm.Gap()

# ## Question
#
# A pair of charged objects with -0.2C and +0.2C of charge are connected with an ammeter. If they dischage (so they both end up having a net charge of zero) over 0.1 seconds:
#
# a) How many electrons will move through the wire?
#
# b) What is the average current through the Ammeter?
#
# c) What is the direction of conventional current?
#
# d) What is the rate of electrons passing through the Ammeter per second?
#

# ## Voltage
#
#

# +
## Drawing circuits

When drawing diagrams of electrical circuits, a wire is a straight line, and Ammeters are circles with an "A" inside. 

A Voltm
